﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeHolder
{
    public float expense;
    public NodeHolder destination;
    public Node node;

    public NodeHolder(float e, Node n)
    {
        expense = e;
        node = n;
    }

    public void SetNext(NodeHolder node)
    {
        destination = node;
    }
    public NodeHolder Next()
    {
        return destination;
    }
    public void setExpense(float e)
    {
        expense = e;
    }
    public float getExpense()
    {
        return expense;
    }
}

public class NodeList
{

    public NodeHolder root;

    public NodeList()
    {
        root = new NodeHolder(0, null);
    }

    public void add(Node n)
    {
        //Debug.Log("Adding " + n.location.x + " " + n.location.y);
        NodeHolder curr = root;
        if (curr.Next() == null)
        {
            curr.SetNext(new NodeHolder(n.getExpense(), n));
            return;
        } else
        {
            while (curr != null)
            {
                if (n.getExpense() > curr.getExpense())
                {
                    curr = curr.Next();
                }
                else
                {
                    NodeHolder newnode = new NodeHolder(n.getExpense(), n);
                    newnode.SetNext(curr.Next());
                    curr.SetNext(newnode);
                    break;
                }
            }
        }

    }
    public Node pop()
    {
        if (root.Next() == null)
        {
            return null;
        }

        NodeHolder ret = root.Next();
        

        root.SetNext(ret.Next());
        ret.SetNext(null);
        return ret.node;
    }

    public override string ToString()
    {
        string ret = "";
        NodeHolder curr = root.Next();
        while (curr != null)
        {
            ret += curr.node.location.x + "," + curr.node.location.y + " " + curr.expense;
            curr = curr.Next();
        }
        return ret;
    }

}

public class Path
{

    public Path next;
    public Vector2 location;

    public Path(Vector2 loc)
    {
        location = loc;
    }

    public void SetNext(Path p)
    {
        next = p;
    }
}

public class PathList
{

    public Path root;

    public PathList()
    {
        root = new Path(new Vector2(0,0));
    }

    public void add(Vector2 v)
    {
        
        Path add = new Path(v);
        add.SetNext(root.next);
        root.SetNext(add);
        
    }

    public override string ToString()
    {
        string ret = "";
        Path curr = root.next;
        while (curr != null)
        {
            ret += curr.location.x + "," + curr.location.y + " ";
            curr = curr.next;
        }
        return ret;
    }

    public Path pop()
    {
        if (root.next == null)
        {
            return null;
        }

        Path ret = root.next;
        root.SetNext(ret.next);
        ret.SetNext(null);

        return ret;
    }
}
