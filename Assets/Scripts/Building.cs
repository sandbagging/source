﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Building : MonoBehaviour
{
    public int mobMax = 5;
    public int mobCount = 0;
    public float currentcooldown = 2f;
    public float cooldown = 2f;
    public GameObject mob;      // Unit to be spawned
    public GameObject[] mobs;   // Direct reference to unit spawned (for creation/deletion, etc)
    public Board board;
    private System.Random rand = new System.Random();
    // Use this for initialization
    void Start()
    {
        mobs = new GameObject[mobMax];
    }

    private void OnMouseUp()
    {
        foreach (GameObject thing in mobs) {
            Destroy(thing);
        }
        board.remove((int)transform.localPosition.x, (int)transform.localPosition.y);
    }

    public void setBoard(Board b)
    {
        board = b;
    }

    void KillMe(int index)
    {
        mobCount -= 1;
        Destroy(mobs[index]);
        mobs[index] = null;
    }
    public virtual void updateSprite()
    {

    }
    // Update is called once per frame
    void Update()
    {
        if (mob != null) {
            // Spawn Mobs
            if (mobCount < mobMax)
            {
                currentcooldown -= Time.deltaTime;
                if (currentcooldown <= 0)
                {
                    Vector2 loc = new Vector2(transform.localPosition.x + (float)rand.NextDouble() - 0.5f, transform.localPosition.y + (float)rand.NextDouble() - 0.5f);
                    GameObject newMob = Instantiate(mob, loc, Quaternion.identity) as GameObject;
                    for (int i = 0; i < mobMax; i++)
                    {
                        if (mobs[i] == null) 
                        {
                            mobs[i] = newMob;
                            newMob.SendMessage("Register", this);
                            newMob.SendMessage("Index", i);
                            break;
                        }
                    }
                    mobCount += 1;
                    currentcooldown = cooldown;
                }

            }
        }
    }
}
