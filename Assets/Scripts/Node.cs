﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node {
    public float heuristic;
    public float expense;
    public Vector2 location;
    public Node destination;

    public Node(float x, float y, float h)
    {
        location = new Vector2(x, y);
        heuristic = h;
        expense = 99999;
    }

    public void SetNext(Node node)
    {
        destination = node;
    }
    public Node Next()
    {
        return destination;
    }
    public void setExpense(float e)
    {
        expense = e;
    }
    public float getExpense()
    {
        return expense;
    }
    public float getX()
    {
        return location.x;
    }
    public float getY()
    {
        return location.y;
    }
    public float getHeuristic()
    {
        return heuristic;
    }
}
