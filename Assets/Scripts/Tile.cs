﻿using UnityEngine;
using System.Collections;

public class Tile : MonoBehaviour

{

    /*  1. Set top, bottom, and generate loot if applicable.
     *      note: bottom will always be dirt. if there is no top, then the tile is "pathable".
     *  2. Render and update as needed. 
     *  
     *  How do I get multiple behaviorsets?
     *  1. Have objects that derive Tile
     *  2. Instantiate "Buildings" on top of Tile, that do the behavior. Board references tile, which references 
     */
     
    public GameObject top;
    public GameObject loot;
    private Vector2 location;

	public Tile(Vector2 loc)
	{
        location = loc;
	}

    public bool isPathable()
    {
        return top == null;
    }
    
    public void setTop(GameObject building)
    {
        if ( top == null ) {
            top = Instantiate(building, location, Quaternion.identity) as GameObject;
        } else
        {
            Debug.LogError("Top already exists!");
        }
    }

    void removeTop()
    {
        Destroy(top);
    }
}
