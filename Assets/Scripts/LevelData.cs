﻿using UnityEngine;
using System.Collections;

public class LevelData : MonoBehaviour
{
    private int[,,] levels;
    private int[] level_x;
    private int[] level_y;
    public LevelData()
    {
        levels = new int[,,] { { { 1,1,1,1,1,1,1,1,1 },
                                 { 1,1,1,1,1,1,1,1,1 },
                                 { 1,2,1,1,1,1,1,3,1 },
                                 { 1,0,1,1,1,1,1,0,1 },
                                 { 1,0,0,0,0,0,0,0,1 },
                                 { 1,1,1,1,4,1,1,1,1 },
                                 { 1,1,1,1,1,1,1,1,1 },
                                 { 1,1,1,1,1,1,1,1,1 } } };

        level_x = new int[] { 9 };
        level_y = new int[] { 8 };
    }

    // This is about where I forgot level is actually a word
    public int[,] getLevel(int level)
    {
        int[,] slice = new int[level_y[level], level_x[level]];
        for (int y = level_y[level]-1; y >= 0 ; y--)
        {
            for (int x = 0; x < level_x[level]; x++)
            {
                slice[level_y[level]-y-1, x] = levels[level, y, x];
            }
        }
        return slice;
    }
    public int getLevelX(int i)
    {
        return level_x[i];
    }
    public int getLevelY(int i)
    {
        return level_y[i];
    }
}
