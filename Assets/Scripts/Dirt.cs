﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dirt : Building {
    public Sprite sprites;
    public SpriteRenderer[] sprite;

    void Start()
    {
        board = GameObject.Find("GameEngine").GetComponent<Board>();
        sprite = GetComponentsInChildren<SpriteRenderer>();
        sprite[0].sprite = sprites;
    }


    private void OnMouseUp()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }


    /* Indices of ret (x marks where getNeighbors() was called)
     *   0  1  2 
     *   3  X  4
     *   5  6  7
     */
    public override void updateSprite()
    {
        sprite[0].sprite = sprites;
    }

}
