﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Minion : MonoBehaviour {
    public float x;
    public float y;
    public float xdir;
    public float ydir;
    public float speed = 1;
    public float debugprinter = 5f;
    public int myindex;
    public Building parent;
    public PathList path;
    public Vector2 destination;
    private Board board;

    public Vector2 closestTile()
    {
        float closestx;
        float closesty;

        int xround = (int)transform.localPosition.x;
        float xdiff = transform.localPosition.x - xround;
        if (xdiff < .5)
        {
            closestx = xround;
        }
        else
        {
            closestx = xround + 1;
        }

        int yround = (int)transform.localPosition.y;
        float ydiff = transform.localPosition.y - yround;
        if (ydiff < .5)
        {
            closesty = yround;
        }
        else
        {
            closesty = yround + 1;
        }
        return new Vector2(closestx,closesty);
    }

	// Use this for initialization
	void Start () {
        board = GameObject.Find("GameEngine").GetComponent<Board>();
        path = board.getPath(closestTile(), board.lairlocation);
        Debug.Log("My path is " + path.ToString());
        destination = path.pop().location;
    }

    void Register(Building p)
    {
        parent = p;
    }
    void Index( int i)
    {
        myindex = i;
    }

    // Update is called once per frame
    void Update () {
        
        x = transform.localPosition.x;
        y = transform.localPosition.y;

        // If we are "close enough" to our destination,
        if ( (Mathf.Abs(x - destination.x) < .3) && (Mathf.Abs(y - destination.y) < .3 ) || path.root.next == null)
        {
            // if our path is empty,
            if ((Mathf.Abs(x - board.lairlocation.x) < .3) && (Mathf.Abs(y - board.lairlocation.y) < .3))
            {
                parent.SendMessage("KillMe", myindex);
            } else if (path.root.next == null)
            {
                path = board.getPath(closestTile(), board.lairlocation);
            } else
            { 
                // Otherwise, get the next location
                destination = path.pop().location;
                // Debug.Log(path.ToString());
            }
        }
        xdir = Mathf.Abs(destination.x - x) / (destination.x - x);
        ydir = Mathf.Abs(destination.y - y) / (destination.y - y);
        
        transform.Translate(new Vector3((xdir * speed * Time.deltaTime), (ydir * speed * Time.deltaTime), 0f));
    }

}
