﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : Building
{
    public SpriteRenderer sprite;
    public int sprid = 0;
    private System.Random rand;

    // Update is called once per frame
    void Update()
    {

    }


    public override void updateSprite()
    {
        int[] n = board.getNeighbors((int)transform.localPosition.x, (int)transform.localPosition.y);
        sprite = GetComponentInParent<SpriteRenderer>();
        rand = new System.Random();
        Debug.Log(transform.localPosition + " " + n);

        int num_adjacent = n[1] + n[3] + n[4] + n[6];
        int num_diagonal = n[0] + n[2] + n[5] + n[7];
        /*
         * 0 1 2
         * 3 X 4
         * 5 6 7
         * 
         * 1 = 2
         * 3 = 4
         * 4 = 3
         * 6 = 1
         */
         if (num_adjacent == 0)
        {
            sprid = 0;
        } else if (num_adjacent == 1) {
            // caps
            sprid = (1 * n[6]) + (2 * n[1]) + (3 * n[4]) + (4 * n[3]);
        } else if (num_adjacent == 2) {
            // bars
            // Direction refers to filled corners.
            int TL = n[1] * n[3];
            int TR = n[1] * n[4];
            int BL = n[6] * n[3];
            int BR = n[6] * n[4];
            sprid = 4 + (1 * n[1] * n[6]) + (2 * n[3] * n[4]) + 
            // corners + fills
           (3 * BR + (4 * BR * n[7])) + (4 * BL + (4 * BL * n[5])) + (5 * TR + (4 * TR * n[2])) + (6 * TL + (4 * TL * n[0]));
        } else if (num_adjacent == 3)
        {
            // T shapes
            // Direction refers to the OPEN end
            int U = n[3] * n[4] * n[6];
            int D = n[1] * n[3] * n[4];
            int L = n[1] * n[4] * n[6];
            int R = n[1] * n[3] * n[6];
            
            sprid = 14 +
                (5  * U - (U * n[5]) - (2 * U * n[7]) - (rand.Next(1, 2) * U * n[5] * n[7])) +
                (10 * D - (D * n[0]) - (2 * D * n[2]) - (rand.Next(1, 2) * D * n[2] * n[0])) +
                (15 * L - (L * n[2]) - (2 * L * n[7]) - (rand.Next(1, 2) * L * n[7] * n[2])) +
                (20 * R - (R * n[0]) - (2 * R * n[5]) - (rand.Next(1, 2) * R * n[5] * n[0]));
        } else if (num_adjacent == 4)
        {
            if (num_diagonal == 0)
            {
                sprid = 52;
            }
            else if (num_diagonal == 1)
            {
                sprid = 45 + (n[7]) + (2 * n[5]) + (3 * n[2]) + (4 * n[0]);
            }
            else if (num_diagonal == 2)
            {
                sprid = 41 + (1 * n[5] * n[7]) + (2 * n[0] * n[2]) + (3 * n[2] * n[7]) + (4 * n[0] * n[5]) + (9 * n[2] * n[5]) + (10 * n[0] * n[7]);
            } else if (num_diagonal == 3)
            {
                sprid = 37 + (1 * n[2] * n[5] * n[7]) + (2 * n[0] * n[5] * n[7]) + (3 * n[0] * n[2] * n[7]) + (4 * n[0] * n[2] * n[5]);
            } else
            {
                sprid = 35 + rand.Next(0, 3);
            }
        }

        sprite.sprite = board.getWall(sprid);
    }
}