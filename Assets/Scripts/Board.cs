﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;       //Allows us to use Lists.
using Random = UnityEngine.Random;      //Tells Random to use the Unity Engine random number generator.


public class Board : MonoBehaviour
{ 
    public int level = 0;
    public Sprite[] walls;
    public GameObject floor;
    public GameObject[] buildings;                                  //Array of wall prefabs.
    public GameObject[,] map;
    public LevelData levels;
    public Vector2 lairlocation;
    private Node[,] paths;
    private int[,] stage;
    private int stage_y;
    private int stage_x;

    //Clears our list gridPositions and prepares it to generate a new board.
    void InitialiseList(int level)
    {
        levels = GetComponent<LevelData>();

        stage = levels.getLevel(level);
        stage_y = levels.getLevelY(level);
        stage_x = levels.getLevelX(level);
        paths = new Node[stage_y, stage_x];
        map = new GameObject[stage_y, stage_x];
        
    }
    public Sprite getWall(int i)
    {
        return walls[i];
    }
    void BoardSetup()
    {

        for (int y = 0; y < stage_y; y++)
        {
            for (int x = 0; x < stage_x; x++)
            {
                if (stage[y, x] == 2)
                {
                    lairlocation = new Vector2(x, y);
                }
                map[y, x] = Instantiate(buildings[stage[y, x]], new Vector2(x, y), Quaternion.identity) as GameObject;
                map[y, x].transform.parent = transform;
                map[y, x].SendMessage("setBoard", this);
            }

        }
        for (int y = 0; y < stage_y; y++)
        {
            for (int x = 0; x < stage_x; x++)
            {
                if (map[y, x].tag.Contains("Upd"))
                {
                    map[y, x].SendMessage("updateSprite");
                }
            }
        }
    }

    public void remove(int x, int y)
    {
        DestroyObject(map[y, x]);
        map[y, x] = Instantiate(buildings[0], new Vector2(x, y), Quaternion.identity) as GameObject;
        map[y, x].transform.parent = transform;
        updateNeighbors(x, y);
    }
  

    //SetupScene initializes our level and calls the previous functions to lay out the game board
    public void SetupScene(int level)
    {
        InitialiseList(level);
        
        BoardSetup();

    }

    public PathList getPath(Vector2 start, Vector2 end)
    {
        PathList ret = new PathList();
        int pathcountd = 0;
        // PREPARE TO BE BOARDED
        for (int y = 0; y < stage_y; y++)
        {
            for (int x = 0; x < stage_x; x++)
            {
                if (map[y, x].tag.Contains("Path"))
                {
                    float dx = Mathf.Abs(x - lairlocation.x);
                    float dy = Mathf.Abs(y - lairlocation.y);
                    
                    float h = (dx + dy);
                    //Debug.Log("Heuristic " + x + " " + y + " : " + h);
                    paths[y, x] = new Node(x, y, h);
                    pathcountd++;
                }

            }
        }
        // ITERATE OVER BOARDED
        NodeList iterate = new NodeList();
        //Debug.Log("Starting with " + start.x + " " + start.y);
        paths[(int)start.y, (int)start.x].setExpense(paths[(int)start.y, (int)start.x].getHeuristic());
        iterate.add(paths[(int)start.y, (int)start.x]); // First node!
        List<Node> pathed = new List<Node>();
        bool found = false;
        int travelCost = 10;
        int diagCost = 14;
        while (!found || iterate.root.Next() != null)
        {
            //Debug.Log(iterate.ToString());
            Node curr = iterate.pop();
            if (curr == null)
            {
                Debug.Log("Null Node Assigned!");
                return null;
            }
            pathed.Add(curr);

            int b = (int)curr.getY();
            int a = (int)curr.getX();
            //Debug.Log("Checking " + a + " " + b);
            // Get nodes adjacent to this node, and update their values
            for (int y = b - 1; y <= b + 1; y++)
            {

                for (int x = a - 1; x <= a + 1; x++)
                {

                    // if it's offstage, not pathable, self, or blocked diagonally, just skip it
                    if (x < 0 || x >= stage_x || y < 0 || y >= stage_y || (y == b && x == a) || paths[y, x] == null ||
                        ((x == a - 1 && y == b - 1) && (paths[b - 1, a] == null || paths[b, a - 1] == null)) || ((x == a + 1 && y == b - 1) && (paths[b - 1, a] == null || paths[b, a + 1] == null)) ||
                        ((x == a - 1 && y == b + 1) && (paths[b + 1, a] == null || paths[b, a - 1] == null)) || ((x == a + 1 && y == b + 1) && (paths[b + 1, a] == null || paths[b, a + 1] == null)) || pathed.Contains(paths[y, x]))
                    {
                        continue;
                    }

                    //Debug.Log("Trying " + x + " " + y);
                    float newExpense = curr.getExpense() + paths[y, x].getHeuristic();

                    if (x != a && y != b)
                    {
                        newExpense += diagCost;
                    }
                    else
                    {
                        newExpense += travelCost;
                    }


                    // If I am the better path, use me instead!
                    if (newExpense < paths[y, x].getExpense())
                    {
                        paths[y, x].setExpense(newExpense);
                        paths[y, x].SetNext(curr);
                        //Debug.Log("Node " + x + " " + y + " points to " + a + " " + b);
                        if (y == end.y && x == end.x)
                        {
                            //Debug.Log("Found the exit!");
                            found = true;
                        }
                    }
                    // If I haven't been tried already, add to list
                    if (pathed.Contains(paths[y, x]) != true)
                    {
                        //Debug.Log("Adding " + x + " " + y);
                        iterate.add(paths[y, x]);
                    }
                }
            }
        }
        
        Node pointer = paths[(int)end.y, (int)end.x];
        while (pointer != null)
        {
            ret.add(pointer.location);
            pointer = pointer.Next();
        }
        //Debug.Log("Path found: " + ret.ToString());
        return ret;
    }

    public int[] getNeighbors(int a, int b)
    {
        int[] ret = { 0, 0, 0, 0, 0, 0, 0, 0 };
        int incr = 0;
        for (int y = b + 1; y >= b - 1; y--)
        {
            for (int x = a - 1; x <= a + 1; x++)
            {
                if (y == b && x == a)
                {
                    continue;
                }
                else if (x < 0  || x >= stage_x || y < 0 || y >= stage_y )
                {
                    ret[incr] = 1; // It's a "wall"
                }
                else if (map[y, x].tag.Contains("Wall")) // Yes, this needed an elif. 
                {
                    ret[incr] = 1; 
                }
                incr += 1;
            }
        }
        return ret;
    }
    /* Indices of ret (x marks where getNeighbors() was called)
     *   0  1  2 
     *   3  X  4
     *   5  6  7
     */
    public void updateNeighbors(int a, int b)
    {
        for (int y = b + 1; y >= b - 1; y--)
        {
            for (int x = a - 1; x <= a + 1; x++)
            {
                
                if (x < 0 || x >= stage_x || y < 0 || y >= stage_y || y == b && x == a || map[y, x] == null)
                {
                    continue; 
                }
                if (map[y, x].tag.Contains("Upd"))
                {
                    map[y, x].SendMessage("updateSprite");
                }
            }
        }
    }

    public Vector2 getLair()
    {
        return lairlocation;
    }
}
